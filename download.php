<?php
session_start();
if(!isset($_SESSION['id']) || $_SESSION['id'] == null){
	header("Location:index.php");
}

if(isset($_POST['file'])){
	$realpath = "users/".$_SESSION['id']."/".$_POST['file'];
	//echo $realpath;
	if(file_exists($realpath)){
		
		header('Content-Description: File Transfer');
    	header('Content-Type: application/octet-stream');
    	header("Content-Disposition: attachment; filename=\"".basename($realpath)."\"");
		header('Expires: 0');
		header("Cache-Control: no-cache, must-revalidate");
    	header("Content-Length: " .(string)(filesize($realpath)));
    	//echo filesize($realpath);
    	readfile($realpath);
    	exit;
	}
	
}

?>