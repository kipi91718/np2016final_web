<?php
	session_start();
	error_reporting(E_ERROR);
	include("connect.php");

	if(!isset($_SESSION['id']) && $_SESSION['id'] == null){
		ob_start();
		echo "<script>alert('Login First !')</script>";
		header("Refresh: 0; url=login.php");
		ob_end_flush();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>View Files</title>
	<style type="text/css">
		body{margin: 30px 30px 10px 30px}
		body{background-image: url(img/bg.gif);}
		a:link{text-decoration:none;}
		a:visited{text-decoration:none; color:MediumTurquoise;}
		a:active{color:Tomato;}
		a:hover{font-size:114%; color:Orange}
		a.top:link{color:white;}
		a.top:visited{color:white;}
		a.top:active{color:white;}
		a.top:hover{font-size:100%;color:white;}
		ul{list-style-image: url(img/item.gif);}

		div{
			position: relative;
			left: 40%;
			margin-left: -100px;
			width: 760px;
		}
		input.but{
			font-size:20px;
			
			border-style:none;
			width:200px;
			cursor:pointer;
			border-radius: 12px;
			box-shadow: 0px 3px LightCoral ;
			background-color:SeaShell ;
			color:#9932CC;
		}
			
		input.but:hover{
			box-shadow:5px 5px 4px;
			transition-duration: 0.3s;
			background-color:Black;
			color:white;
		}

		tr td{
			width: 120px;
			border-style:none;
			text-align:center;
		}
	</style>
</head>
<body>
<a href="index.php"><img src="img/home.png" alt="back to home page"></a>
<?php  
	$pattern = '/\.$/';
	$path = "./users/".$_SESSION['id'];
	chdir($path);

	echo "<br><h2>Hello ".$_SESSION['id'].", here are your files.</h2><br>";

	function getDirFile($path){
	if($file_handler=opendir($path)){
		while(false !== ($file=readdir($file_handler))){

			if($file!="." && $file!=".."){
				if(is_dir("$path/$file")){

					if(substr_count("$path/$file","/")>1){
						$count=str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;",substr_count("$path/$file","/"));
						//echo $count.$file;
						//echo "path:".$path.$count." dir:".$file;
						echo "<h4 style = 'color: DarkGray '>".$count."<img src='img/dir.png' alt='Dir:'> ".$file;
						
						echo "</h4>\n";
					}else{
						//echo $file;
						echo "<h4 style = 'color: DarkGray '><img src='img/dir.png' alt='Dir:'>".$file."</h4>\n";
					}
					//echo "<br/>";
					getDirFile("$path/$file");

				}else{

					if(substr_count("$path/$file","/")>1){
						$count=str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;",substr_count("$path/$file","/"));
						$p_path = substr($path,2);
						echo "<form method = \"post\" action = \"download.php\" >\n";
						echo "<h4>".$count."<img src='img/Doc.png' alt='File:'>".$file."\n";
						echo "<input type = \"hidden\" name=\"file\" value=\"".$p_path."/".$file."\">\n";
						echo "<input type =\"submit\"  value=\"download\"></h4>\n</form>\n";
						
					}else{
						//echo "<h4>File:".$file."</h4>\n";
						echo "<form method =  \"post\" action = \"download.php\" >\n";
						echo "<h4><img src='img/Doc.png' alt='File:'>".$file."\n";
						echo "<input type = \"hidden\" name=\"file\" value=\"".$file."\">\n";
						echo "<input type =\"submit\"  value=\"download\"></h4>\n</form>\n";
					}
					//echo "<br/>";


					}

				}
			}
		}
	}

	getDirFile(".");

	//$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::CATCH_GET_CHILD);
	//iterateDir($objects);
	/*foreach($objects as $name => $object){
    	
    	echo $name."<br>";
	}*/

?>
</body>
</html>