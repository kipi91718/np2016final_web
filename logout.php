<?php
	session_start();
	session_destroy();
	ob_start();
	echo "<script>alert('Logout OK !')</script>";
	header("Refresh: 0; url=index.php");
	ob_end_flush();
?>