<?php
	session_start();
	error_reporting(E_ERROR);
	include("connect.php");

	if(isset($_SESSION['id']) && $_SESSION['id'] != null){
		header("Refresh: 0; url=index.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="img/logo.ico">
	<style type="text/css">
		body{margin: 0px 30px 30px 30px}
		body{background-image: url(img/bg.gif);}
		a:link{text-decoration:none;}
		a:visited{text-decoration:none; color:MediumTurquoise;}
		a:active{color:Tomato;}
		a:hover{font-size:114%; color:Orange}
		a.top:link{color:white;}
		a.top:visited{color:white;}
		a.top:active{color:white;}
		a.top:hover{font-size:100%;color:white;}
		ul{list-style-image: url(img/item.gif);}

		div{
			position: relative;
			left: 40%;
			margin-left: -100px;
			width: 760px;
		}
		input.but{
			font-size:20px;
			
			border-style:none;
			width:200px;
			cursor:pointer;
			border-radius: 12px;
			box-shadow: 0px 3px LightCoral ;
			background-color:SeaShell ;
			color:#9932CC;
		}
			
		input.but:hover{
			box-shadow:5px 5px 4px;
			transition-duration: 0.3s;
			background-color:Black;
			color:white;
		}

		tr td{
			width: 120px;
			border-style:none;
			text-align:center;
		}
		
	</style>
</head>
<body>
<?php
	
	$pattern = '/[a-zA-Z0-9_]{1,50}/';
	if(!isset($_POST['id']) && !isset($_POST['password']) && !isset($_POST['password2'])){
		echo '<script>alert("all fields can\'t be empty");</script>';

	}else{
		$acc = $_POST['id'];
		$acc = strtolower($acc);
		$pwd = $_POST['password'];
		$pwd2 = $_POST['password2'];

		if(!empty($acc) && !empty($pwd) && !empty($pwd2)){
			if(preg_match($pattern, $acc) == 1 && preg_match($pattern,$pwd) == 1 && preg_match($pattern,$pwd2) == 1){
				$sql = $dbh->prepare("SELECT * FROM user_list WHERE username = :acc");
				$sql->bindParam(':acc', $acc, PDO::PARAM_STR);
				$sql->execute();
				$result = $sql->fetchAll(PDO::FETCH_ASSOC);
				
				if(count($result) > 0){
					echo '<script>alert("repeated username");</script>';
				}else if($pwd != $pwd2){
					echo '<script>alert("the 2 password cannot match");</script>';
				}
				else{
					
					$sql = $dbh->prepare("INSERT INTO user_list (username,password) VALUES (:acc,:passwd)");
					$sql->bindParam(':acc', $acc, PDO::PARAM_STR);
					$sql->bindParam(':passwd', $pwd , PDO::PARAM_STR);
					$sql->execute();

					$sql2 = "CREATE TABLE ".$acc."_file_list (serial int primary key auto_increment, filename varchar(32) NOT NULL, checksum char(40) NOT NULL)";
					$dbh->exec($sql2);
					ob_start();
					echo "<script>alert('Register OK !')</script>";
					header("Refresh: 0; url=index.php");
					ob_end_flush();

				}

			}
			else{
				echo '<script>alert("the format of username or password are not allowed");</script>';
			}
		}else{
			echo '<script>alert("all fields can\'t be empty");</script>';
		}
	}

?>

<br><br>
<div id="all">
<img src="img/sign-up.png" alt="sign-up">
<form action="" method="post">
	<h3>Username : <input type="text" name="id" placeholder="account"></h3>
	<h3>Password : <input type="password" name="password" placeholder="password"></h3>
	<h3>Password again : <input type="password" name="password2" placeholder="password again"></h3>
	<table>
		<tr>
			<td><input type="submit" class="but" value="Submit"></td>
			<td><input type="reset" class="but" value="reset"></td>
		</tr>
		<tr>
			<td colspan="2"><a href="index.php"><img src="img/home.png" alt="back to home page"></a></td>
		</tr>
	</table>
</form>

</div>
</body>
</html>