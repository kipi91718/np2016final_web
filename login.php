<?php
	session_start();
	error_reporting(E_ERROR);
	include("connect.php");

	if(isset($_SESSION['id']) && $_SESSION['id'] != null){
		header("Refresh: 0; url=index.php");
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="img/logo.ico">
	<style type="text/css">
		body{margin: 30px 30px 10px 30px}
		body{background-image: url(img/bg.gif);}
		a:link{text-decoration:none;}
		a:visited{text-decoration:none; color:MediumTurquoise;}
		a:active{color:Tomato;}
		a:hover{font-size:114%; color:Orange}
		a.top:link{color:white;}
		a.top:visited{color:white;}
		a.top:active{color:white;}
		a.top:hover{font-size:100%;color:white;}
		ul{list-style-image: url(img/item.gif);}

		div{
			position: relative;
			left: 40%;
			margin-left: -100px;
			width: 760px;
		}
		input.but{
			font-size:20px;
			
			border-style:none;
			width:200px;
			cursor:pointer;
			border-radius: 12px;
			box-shadow: 0px 3px LightCoral ;
			background-color:SeaShell ;
			color:#9932CC;
		}
			
		input.but:hover{
			box-shadow:5px 5px 4px;
			transition-duration: 0.3s;
			background-color:Black;
			color:white;
		}

		tr td{
			width: 120px;
			border-style:none;
			text-align:center;
		}
		
	</style>
</head>
<body>
<div id="all">
<img src="img/login.png" alt="Login">
<?php
	$pattern = '/[a-zA-Z0-9_]{1,50}/';
	if(!isset($_POST['account']) && !isset($_POST['password'])){
		echo '<script>alert("all fields can\'t be empty");</script>';
	}else{
		$acc = $_POST['account'];
		$acc = strtolower($acc);
		$pwd = $_POST['password'];
		if(!empty($acc) && !empty($pwd)){

			if(preg_match($pattern, $acc) == 1 && preg_match($pattern, $pwd) == 1){
				$sql = $dbh->prepare("SELECT * FROM user_list WHERE username = :acc AND password = :pwd");
				$sql->bindParam(':acc', $acc, PDO::PARAM_STR);
				$sql->bindParam(':pwd', $pwd, PDO::PARAM_STR);
				$sql->execute();
				$result = $sql->fetchAll(PDO::FETCH_ASSOC);
				if(count($result) == 0){
					echo '<script>alert("Login Fail");</script>';
				}else{
					$_SESSION['id'] = $acc;
					ob_start();
					echo "<script>alert('Login OK !')</script>";
					header("Refresh: 0; url=index.php");
					ob_end_flush();
				}

			}else{
				echo '<script>alert("some problem of format on your input");</script>';
			}

		}else{
			echo '<script>alert("all fields can\'t be empty");</script>';
		}
	}
	
?>

<form action="" method="post">
<h2 style="color:Purple;font-weight:Bold;">If you have an account , please login</h2>

<h3>Username:<input type="text" class="data" name="account" size="50" autofocus="autofocus"></h3>

<h3>Password:<input type="password" class="data" name="password" size="50"></h3>
<table>
	<tr>
		<td ><input type="submit" class="but" value="Submit"></td>
		<td ><input type="reset" class="but" value="Reset"></td>
	</tr>
</table>

<br>
<h3>If you don't have an account, please <a href="register.php">register</a></h3>
<a href="index.php"><img src="img/home.png" alt="back to home page"></a>
		
</form>
</div>
</body>
</html>