<?php
	session_start();
	include("connect.php")
?>
<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="img/logo.ico">
	<style type="text/css">
		body{margin: 0px 30px 30px 30px}
		body{background-image: url(img/bg.gif);}
		a:link{text-decoration:none;}
		a:visited{text-decoration:none; color:MediumTurquoise;}
		a:active{color:Tomato;}
		a:hover{font-size:114%; color:Orange}
		a.top:link{color:white;}
		a.top:visited{color:white;}
		a.top:active{color:white;}
		a.top:hover{font-size:100%;color:white;}
		ul{list-style-image: url(img/item.gif);}
	</style>
</head>
<body>
	<br>
	<img src="img/hello.jpg" alt="Hello">
	<?php
		if(isset($_SESSION['id'])  && $_SESSION['id'] != null){
			echo "<h2>Nice to see you ,".$_SESSION['id']." !![<a href='logout.php'>Logout</a>]</h2>";
		}
	?>
	<h2>Here are the functions you can use on web client NOW</h2>
	<br>
	<ul>
		<?php

			if(isset($_SESSION['id']) && $_SESSION['id'] != null ){
				echo "<li><h3><a href='viewfiles.php'>View File List and Download File</h3></a></li>";
			}else {
				echo "<li><h3>Register New Account(<a href='register.php'>register page</a>)</h3></li>";
				echo "<li><h3>Login(<a href='login.php'>login page</a>)</h3></li>";
				echo "<li><h3>View File List and Download File (after login)</h3></li>";
			}

		?>
		
		
	</ul>

</body>
</html>